"""
@file vend_0x01.py

@brief This file is a Finite State Machine serving as a vending machine.
@details In this program, the user can exchange any coin or bill, up to a $20 bill, for several different drinks. The user can
also check their balance and recieve change.

@author Cole Andrews
"""

import keyboard

## init state
S0_INIT = 0

## idle state, checks for keypresses
S1_idle = 1

## checks balance when item is selected
S2_checkBalance = 2

## defining variable to store key presses
last_key = ''

# storing balance as a list
balance = [0, 0, 0, 0, 0, 0, 0, 0]

## price of item
price = 0

## setting initial state
state = S0_INIT

def kb_cb(key):
    """ 
    Callback function which is called when a key has been pressed.
    """
    global last_key
    last_key = key.name
    
## defining allkeys to be presses
keyboard.on_release_key("0", callback=kb_cb)
keyboard.on_release_key("1", callback=kb_cb)
keyboard.on_release_key("2", callback=kb_cb)
keyboard.on_release_key("3", callback=kb_cb)
keyboard.on_release_key("4", callback=kb_cb)
keyboard.on_release_key("5", callback=kb_cb)
keyboard.on_release_key("6", callback=kb_cb)
keyboard.on_release_key("7", callback=kb_cb)
keyboard.on_release_key("c", callback=kb_cb)
keyboard.on_release_key("p", callback=kb_cb)
keyboard.on_release_key("s", callback=kb_cb)
keyboard.on_release_key("d", callback=kb_cb)
keyboard.on_release_key("e", callback=kb_cb)
    
def getBalance(balance):
    '''
    @brief takes balance as a list and converts to an int
    
    returns int balance
    '''
    b = (balance[0]*1 + balance[1]*5 + balance[2]*10 + balance[3]*25 + balance[4]*100 + balance[5]*500 + balance[6]*1000 + balance[7]*2000)
    return b
    
def getChange(price, payment):
    
    '''
    @brief Calculates the change for an item given the price as an int and the payment as a list

    returns listChange
    
    '''
    listChange = []
    intChange = (payment[0]*1 + payment[1]*5 + payment[2]*10 + payment[3]*25 + payment[4]*100 + payment[5]*500 + payment[6]*1000 + payment[7]*2000) - price
    
    if intChange >= 2000:
        listChange = [((intChange - intChange%2000)/2000)]  + listChange
        intChange = intChange%2000
    else:
        listChange = [0]  + listChange
        
    if intChange >= 1000:
        listChange = [((intChange - intChange%1000)/1000)]  + listChange
        intChange = intChange%1000 
    else:
        listChange = [0]  + listChange
        
    if intChange >= 500:
        listChange = [((intChange - intChange%500)/500)]  + listChange
        intChange = intChange%500
    else:
        listChange = [0]  + listChange
        
    if intChange >= 100:
        listChange = [((intChange - intChange%100)/100)]  + listChange
        intChange = intChange%100 
    else:
        listChange = [0]  + listChange
        
    if intChange >= 25:
        listChange = [((intChange - intChange%25)/25)]  + listChange
        intChange = intChange%25
    else:
        listChange = [0]  + listChange
        
    if intChange >= 10:
        listChange = [((intChange - intChange%10)/10)]  + listChange
        intChange = intChange%10 
    else:
        listChange = [0]  + listChange
        
    if intChange >= 5:
        listChange = [((intChange - intChange%5)/5)]  + listChange
        intChange = intChange%5 
    else:
        listChange = [0]  + listChange
        
    if intChange >= 1:
        listChange = [((intChange - intChange%1)/1)]  + listChange
        intChange = intChange%1
    else:
        listChange = [0]  + listChange
        
    
    return listChange

def printWelcome():
    '''
    @brief prints welcome statement for user
    '''
    print('-----------------------------------------------------------------')
    print('Welcome to vendatron, prices are listed below as follows: ')
    print(' C, Cuke: $1.25 | P, Popsi: $1.00 | S, Spryte: $1.50 | D, Dr. Pupper $1.99') 
    print('Please  enter you change as follows:')
    print('0= $0.01, 1 = $0.05 ,2 = $0.10, 3 = $0.25, 4 = $1.00, 5 = $5.00, 6 = $10.00, 7 = $20.00')
    print('Press ''E'' to eject change at anytime.')
    print('-----------------------------------------------------------------')
    
while True:
    '''
    runs one iteration of task every loop
    '''
    
    if(state == S0_INIT):
        state = S1_idle
        printWelcome()
    
    elif(state == S1_idle):
        
        if last_key == '0':
            last_key = ''
            balance[0] = balance[0] + 1
            print('your balance is $' + str(getBalance(balance)/100))
        elif last_key == '1':
            last_key = ''
            balance[1] = balance[1] + 1
            print('your balance is $' + str(getBalance(balance)/100))
        elif last_key == '2':
            last_key = ''
            balance[2] = balance[2] + 1
            print('your balance is $' + str(getBalance(balance)/100))
        elif last_key == '3':
            last_key = ''
            balance[3] = balance[3] + 1
            print('your balance is $' + str(getBalance(balance)/100))
        elif last_key == '4':
            last_key = ''
            balance[4] = balance[4] + 1
            print('your balance is $' + str(getBalance(balance)/100))
        elif last_key == '5':
            last_key = ''
            balance[5] = balance[5] + 1
            print('your balance is $' + str(getBalance(balance)/100))
        elif last_key == '6':
            last_key = ''
            balance[6] = balance[6] + 1
            print('your balance is $' + str(getBalance(balance)/100))
        elif last_key == '7':
            last_key = ''
            balance[7] = balance[7] + 1
            print('your balance is $' + str(getBalance(balance)/100))
        elif last_key == 'c':
            print('Cuke selected')
            last_key = ''
            price = 125
            state = S2_checkBalance
        elif last_key == 'p':
            print('Popsi selected')
            last_key = ''
            price = 100
            state = S2_checkBalance
        elif last_key == 's':
            print('Spryte selected')
            last_key = ''
            price = 150
            state = S2_checkBalance
        elif last_key == 'd':
            print('Dr. Pupper selected')
            last_key = ''
            price = 199
            state = S2_checkBalance
        elif last_key == 'e':
            last_key = ''
            print('Ejecting change')
            balance = [0, 0, 0, 0, 0, 0, 0, 0]
            printWelcome()
            state = 1
        else:
            pass
    
    elif(state == S2_checkBalance):
        if(price > getBalance(balance)):
            print('insufficent funds, your current balance is $' + str(getBalance(balance)/100))
            state = S1_idle
        else:
            balance = getChange(price, balance)
            print('Your change is: ' + str(getBalance(balance)/100))
            state = S1_idle
    
    else:
        pass
            
            
            

    
