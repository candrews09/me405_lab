# -*- coding: utf-8 -*-
"""
@file Frontend_0x03.py

@brief This file serves as the main interface between the user and main_0x03.py
@details This file runs on the user's PC and recieves data from main_0x03.py. Once the data is recieved, it foormats, plots, and saves
the data in a csv file
"""

import serial
import time
import matplotlib.pyplot as plt
import csv

## creating a serial port to communciate with nucleo
cereal = serial.Serial(port = 'com3', baudrate=115273,timeout=1)

def sendChar():
    '''
    @details This function sends and recieves data from a nucleo, strips the data,
    plots it and saves it as a CSV.
    '''
    inv = input('press G to start recording data, Data will record for 5 seconds before plotting, push the button to observe output.')
    if(inv == 'g'):
        cereal.write(str(inv).encode('ascii'))
        
        ## waiting for data, idealy this should be an if statement of some kind to detect write
        time.sleep(5) 
        
        ## receieving and splitting data
        rawData = cereal.readall().decode('ascii')
        rawData = rawData.strip().replace('''array('H', ''','').replace('''array('f',''','').replace('(','').replace(')','')
        rawData = rawData.strip().replace('[','').replace(']','').split('/')
        volts = [float(s) for s in rawData[0].split(',')]
        volts = [3.3*val/4095 for val in volts]
        timeData = [float(s)*1000 for s in rawData[1].split(',')]
        
        ## plotting data
        plt.plot(timeData, volts)
        ax=plt.axes()
        ax.grid()
        
        plt.title('Button Voltage vs Reference Voltage')
        plt.ylabel('Voltage, (V)')
        plt.xlabel('Time, (ms)')
        plt.legend()
        plt.show

        ## saving data into CSV, could not figure out how to put it in columns
        with open('data.csv', 'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(volts)
            writer.writerow(timeData)
        
        return volts

## runs 10 times
for n in range(1):
    print(sendChar())
    
cereal.close()