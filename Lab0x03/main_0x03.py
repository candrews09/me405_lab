# -*- coding: utf-8 -*-
"""
@file main_0x03.py

@brief This file interacts with a physical push button to record ADC data and send it to frontend_0x03.py.

@details It is intended to be used with a NUCLEO-L476RG board and requires wiring pin PC13 to pin PA0. 
"""

import pyb 
from pyb import UART
import micropython
import array

## serial com with PC
myuart = UART(2)

# create emergency buffer to store errors that are thrown inside ISR
micropython.alloc_emergency_exception_buf(200)

## defining frequency to record data and range of array
freq = 100000
ran = 500

## creating time array based off freq and range
time = array.array('f', (0 for index in range (ran)))
n = 0
while n < ran:
    time[int(n)] = n/freq
    n += 1

## create an array to store voltage data
buffy = array.array ('H', (0 for index in range (ran)))

## creating a pin for ref voltage, also an LED for visual test
p0 = pyb.Pin(pyb.Pin.cpu.A0)

## creating an anolog object for a pin
adc = pyb.ADC(p0)

while True:
    ## check if 'g' has been pressed by user
    if myuart.readchar() == 103 or myuart.readchar == 71:
        
        while True:
            ## checking for button press
            if adc.read() == 0: # button is pressed
                
                ## need if statenment for button release or else its all 0s
                if adc.read() > 0:
                    ## reading adc data
                    adc.read_timed(buffy, freq)  
                    break
    
        ## sending all data to Front end
        myuart.write(str(buffy) + '/' + str(time))
        

        
