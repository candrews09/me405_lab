# -*- coding: utf-8 -*-
"""
@file reaction_0x02.py

@brief This is the main.py file for Lab 0x02 and is intended to run on the NUCLEO

@details This program utilizes timers ann interupts to test the user's reaction time,
    The NUCLEO will wait a random time between 2 and 3 seconsd before flashing an LED.
    Once flashed, the program will measure the time until the user presses the button, 
    triggering an interupt, and store the value in an array. When the program is terminated,
    the average value of the user's reaction time is displayed.
    
@author Cole Andrews
"""

import pyb
import micropython
import random
import array

# create emergency buffer to store errors that are thrown inside ISR
micropython.alloc_emergency_exception_buf(200)

## Pin object to use for LED. Attached to PA05
LED = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)

## do I need to create a pin object for button? no I think thats extent

times = array.array('f')

# I think this needs to use extint
def timeCallback(timSource):
    # appending time to list, time is wrong
    time = timer.counter()
    print(time)
    times.append(time)
    # turning LED back off
    LED.value(0)
    pyb.delay(1000)
    
# need to change freq, why 6?
timer = pyb.Timer(2, prescaler = 79, period = 0x7FFFFFFF)

# creating interupt, for falling edge, check pull (PULL_NONE)
extint = pyb.ExtInt(pyb.Pin.cpu.C13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, timeCallback)

while True:
    
    try:
        # random number between 2000 and 3000 in ms
        rng = random.randint(2000,3000)
        pyb.delay(rng)
        LED.value(1)
        print('LED on')
    
        # ctrl + c interrupt
    except KeyboardInterrupt:
        print('Interupt Recognized')
        print(sum(times)/len(times))

        break