function deriv = ode_solver(time, state)
display(state(2))

A = 1
B = 1
T = 0

velocity = state(1)
omega = state(2)
position = state(3)                         
angle = state(4)

%
deriv(1,1) = A*velocity + B*T
deriv(2,1) = velocity

deriv(3,1) = A*omega + B*T
deriv(4,1) = omega

 